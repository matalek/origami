(* Aleksander Matusiak *)
(* Origami *)

(* Punkt na płaszczyźnie *)
type point = float*float

(* Poskładana kartka: ile razy kartkę przebije szpilka wbita w danym punkcie *)
type kartka = point -> int

(* [prostokat p1 p2] zwraca kartkę, reprezentującą domknięty prostokąt
o bokach równoległych do osi układu współrzędnych i lewym dolnym rogu [p1]
a prawym górnym [p2]. *)
let prostokat ((x1,y1):point) ((x2,y2):point) :kartka =
    fun ((x3,y3):point) ->
      if x1 <= x3 && x3 <= x2 && y1 <= y3 && y3 <= y2 then
	 1
      else
	 0
    
(* [kolko p r] zwraca kartkę, reprezentującą kółko domknięte 
o środku w punkcie [p] i promieniu [r] *)
let kolko (s:point) r :kartka =
  fun (x:point) ->
    let odl (x1,y1) (x2,y2) = (* kwadrat odległości p1 od p2 *)
      (x2-.x1)*.(x2-.x1)+.(y2-.y1)*.(y2-.y1)
    in
      if odl s x <= r*.r then
	1
      else
	0

(* iloczyn wektorowy *)
let det (x1,y1) (x2,y2) (x3,y3) = 
  (x2 -. x1)*.(y3 -. y1) -. (x3 -. x1)*.(y2 -. y1)

(* procedura zwracająca współrzędne oraz długość wektora prostopadłego 
do prostej wyznaczonej przez punkty p1 = (x1,y1) i p2 = (x2.y2) 
oraz skierowanego w stronę p3 = (x3,y3) *)
let prostopadly (x1,y1) (x2,y2) (x3,y3) = 
  let (ab1,ab2) = (x2-.x1,y2-.y1) (* współrzędne wektora p1p2 *)
  in let dlu = sqrt(ab1*.ab1 +. ab2*.ab2) (* długość wektora *)
     in if det (x1,y1) (x2,y2) (x3,y3) > 0. then
        (ab2,-.ab1,dlu) 
     else
        (-.ab2,ab1,dlu) 

(* procedura zwracająca parę (a,b), gdzie prosta przechodząca przez punkty
(x1,y1) i (x2,y2) ma równianie y = ax+b *)
let prosta (x1,y1) (x2,y2) = 
  let a = (y1 -. y2)/.(x1 -. x2)
  in (a,y1 -. x1*.a)

(* odbicie punkty p3 = (x3,y3) względem prostej wyznaczonej przez punkty 
p1 = (x1,y1) i p2 = (x2.y2) *)
let odbicie ((x1,y1):point) ((x2,y2):point) ((x3,y3):point) :point =
  if x1 = x2 then
    (2.*.x1-.x3, y3)
  else
    let (u1,u2,dlu) = prostopadly (x1,y1) (x2,y2) (x3,y3)
    and (a,b) = prosta (x1,y1) (x2,y2)
    in let odl = (abs_float (a*.x3 -. y3 +. b) )/.(sqrt (a*.a +. 1.)) (*odległość p3 od p1p2 *)
       in (x3 +. 2.*.odl*.u1/.dlu, y3 +. 2.*.odl*.u2/.dlu) 

(* [zloz p1 p2 k] składa kartkę [k] wzdłuż prostej przechodzącej przez
punkty [p1] i [p2] *)
let zloz ((x1,y1):point) ((x2,y2):point) (f:kartka) :kartka =
  fun (p3:point) ->
    let p4  = odbicie (x1,y1) (x2,y2) p3
    in if p3 = p4 then
         f p3
       else if det (x1,y1) (x2,y2) p3 > 0. then
               f p3 + f p4
	    else 0

(* [skladaj [(p1_1,p2_1);...;(p1_n,p2_n)] k = zloz p1_n p2_n (zloz ... (zloz p1_1 p2_1 k)...)] *)
let skladaj l k =
  List.fold_left 
    (fun a (p1,p2) ->
      zloz p1 p2 a)
    k
    l
