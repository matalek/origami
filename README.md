#Origami#
## Project created as an assignment for *Introduction to Programming (functional approach)* course ##

### Task ###
Implement a library for origami fans to calculate, how many layers does a folded sheet of paper have. The library should implement the interface included in `origami.mli`.

### Grade ###
For this assignment I have achieved the highest possible grade.